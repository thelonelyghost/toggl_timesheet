module TogglTimesheet
  class EntriesCombiner
    def self.call(entries)
      hash = Hash.new([])

      entries.each do |e|
        hash["#{e.date}||#{e.user_id}||#{e.description}"] << e
      end

      hash.map do |key, value|
        entry = value.first.dup
        entry.duration = value.inject(0) do |dur, e|
          dur + e.instance_variable_get(:@duration)
        end

        entry
      end
    end
  end
end
