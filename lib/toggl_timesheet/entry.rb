module TogglTimesheet
  class Entry
    attr_accessor :id, :user_id, :description, :date, :duration, :client, :project, :tags

    def initialize(opts={})
      self.id = opts.fetch(:id, nil)
      self.user_id = opts.fetch(:user_id, nil)
      self.description = opts.fetch(:description, nil)
      self.date = Date.iso8601(opts.fetch(:date, nil)).strftime('%Y-%m-%d')
      self.duration = opts.fetch(:duration, 0)
      self.client = opts.fetch(:client, nil)
      self.project = opts.fetch(:project, nil)
      self.tags = opts.fetch(:tags, nil)
    end

    # duration rounded up to the nearest 15 minute mark
    def duration
      mark = 4.0 # quarter hour (15 min)
      ((@duration.to_f / (60 * 60 * 1000)) * mark).ceil / mark.to_f
    end
  end
end
