module TogglTimesheet
  class User
    attr_accessor :id, :email, :admin, :active, :name

    def initialize(opts={})
      self.id = opts.fetch(:id) { nil }
      self.email = opts.fetch(:email) { nil }
      self.admin = opts.fetch(:admin) { false }
      self.active = opts.fetch(:active) { false }
      self.name = opts.fetch(:name) { nil }
    end
  end
end
