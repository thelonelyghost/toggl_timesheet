module TogglTimesheet
  class Workspace
    attr_accessor :id, :name, :admin, :users

    def initialize(opts = {})
      self.id = opts.fetch(:id) { nil }
      self.name = opts.fetch(:name) { nil }
      self.admin = opts.fetch(:admin) { false }
      self.users = opts.fetch(:users) { [] }
    end
  end
end
