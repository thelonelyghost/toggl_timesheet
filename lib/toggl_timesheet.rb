require File.expand_path('toggl_timesheet/version', File.dirname(__FILE__))
require File.expand_path('toggl_timesheet/user', File.dirname(__FILE__))
require File.expand_path('toggl_timesheet/entry', File.dirname(__FILE__))
require File.expand_path('toggl_timesheet/entries_combiner', File.dirname(__FILE__))
require File.expand_path('toggl_timesheet/workspace', File.dirname(__FILE__))

require 'csv'
require 'json'
require 'net/http'
require 'uri'
require 'rest-client'

module TogglTimesheet
  @token = ENV['API_TOKEN']

  def self.main
    self.users.inject(Hash.new([])) do |data, user|
      data[user.id] = self.entries_for_user(user)
      data
    end
  end

  def self.entries_json
    @entries_json ||= begin
      get_entries = proc do |page|
        uri = URI.parse('https://toggl.com/reports/api/v2/details')
        uri.query = URI.encode_www_form(
          user_agent: 'opensource@thelonelyghost.com',
          workspace_id: self.workspace_id,

          since: ENV['START_DATE'],
          until: ENV['STOP_DATE'],
          page: page
        )
        response = self.get_request(uri)

        JSON.parse(response.body)
      end
      out = []

      page = 1

      loop do
        json_response = get_entries.call(page)
        out += json_response.fetch('data', [])

        break if json_response['total_count'] <= json_response['per_page'] * page || json_response.fetch('data', []).empty?
        sleep 1
      end

      out
    end
  end

  def self.entries
    @entries ||= begin
      self.entries_json.map {|e|
        Entry.new(
          id: e['id'],
          user_id: e['uid'],
          description: e['description'],
          duration: e['dur'],
          date: e['start'],
          client: e['client'],
          project: e['project'],
          tags: e['tags']
        )
      }.reject {|e|
        e.tags.include?('non-billable')
      }.sort {|a,b|
        a.date <=> b.date
      }
    end
  end

  def self.entries_combined
    @entries_combined ||= EntriesCombiner.call(self.entries)
  end

  def self.get_request(uri, limit = 10)
    response = nil
    Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') do |http|
      request = Net::HTTP::Get.new(uri)
      #request['Accept'] = 'application/json'
      #request['Content-Type'] = 'application/json'
      request.basic_auth @token, 'api_token'

      response = http.request(request)
    end
    case response
    when Net::HTTPSuccess then
      response
    when Net::HTTPRedirection then
      location = response['location']
      warn "Redirected to location '#{location}'"
      self.get_request(URI.parse(location), limit - 1)
    else
      warn "Got #{response.class} ... URI was '#{uri}'"
      # raise error unless HTTP status is 2xx
      response.value
    end
  end

  def self.entries_for_user(user)
    entries.select {|entry| entry.user_id == user.id }
  end

  def self.workspace_id
    @workspace ||= self.workspaces.find {|w| w.name == ENV['WORKSPACE_NAME'] }
    @workspace.id
  end

  def self.workspaces
    @workspaces ||= begin
      uri = URI.parse('https://www.toggl.com/api/v8/workspaces')
      response = self.get_request(uri)
      JSON.parse(response.body).map do |w|
        Workspace.new(
          id: w['id'],
          email: w['email'],
          admin: w['admin'],
          active: w['active'],
          name: w['name']
        )
      end
    end
  end

  def self.users
    @users ||= begin
      uri = URI.parse("https://toggl.com/api/v8/workspaces/#{self.workspace_id}/workspace_users")
      response = self.get_request(uri)
      JSON.parse(response.body).map do |u|
        User.new(
          id: u['uid'],
          email: u['email'],
          admin: u['admin'],
          active: u['active'],
          name: u['name']
        )
      end
    end
  end

  def self.write_to_csv(filename='./entries.csv')
    CSV.open(filename, 'wb') do |csv|
      csv << [
        'id',
        'user_email',
        'description',
        'date',
        'duration (hours)',
        'client',
        'project',
        'tags'
      ]
      self.entries.sort {|a,b| a.date <=> b.date }.each do |entry|
        csv << [
          entry.id,
          self.users.find {|user| user.id == entry.user_id }&.email,
          entry.description,
          entry.date,
          entry.duration,
          entry.client,
          entry.project,
          entry.tags.join('|||')
        ]
      end
    end
  end
end
