require "spec_helper"

describe TogglTimesheet do
  it "has a version number" do
    expect(TogglTimesheet::VERSION).not_to be nil
  end

  it "can list the users in a workspace" do
    expect(TogglTimesheet.users).not_to be_empty
  end

  it "can list the workspaces available" do
    expect(TogglTimesheet.workspaces).not_to be_empty
  end

  it "can list the entries in the given workspace" do
    expect(TogglTimesheet.entries).not_to be_empty
  end

  #it "does something useful" do
  #  expect(false).to eq(true)
  #end
end
