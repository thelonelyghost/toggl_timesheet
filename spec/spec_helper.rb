$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
require "toggl_timesheet"

module TogglTimesheet
  def self.users
    @users ||= JSON.parse(File.read(File.join(File.dirname(__FILE__), '../examples/users.json')))
  end

  def self.workspaces
    @workspaces ||= begin
      filename = File.expand_path('../examples/workspaces.json', File.dirname(__FILE__))
      JSON.parse(filename)
    end
  end

  def self.entries
    @entries ||= begin
      filename = File.expand_path('../examples/entries.json', File.dirname(__FILE__))
      JSON.parse(filename)
    end
  end
end
