# TogglTimesheet

An exporting utility for working with the time tracking service, [Toggl](https://toggl.com).

This gem was created because I didn't like the lack of flexibility with Toggl's reporting system. This tool hooks into the API to pull time entries and export them as a CSV, which can be pulled into Excel for further munging.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'toggl_timesheet'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install toggl_timesheet

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/thelonelyghost/toggl_timesheet. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

